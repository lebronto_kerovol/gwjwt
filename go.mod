module bitbucket.org/lebronto_kerovol/gwjwt

go 1.16

require (
	bitbucket.org/lebronto_kerovol/gwerror v0.0.0-20210720211604-bf02946bdaf8
	github.com/golang-jwt/jwt/v4 v4.0.0
)
