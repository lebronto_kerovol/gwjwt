package constants

const (
	CODE_METHOD_HS256 = `hmac sha256`
	CODE_METHOD_HS384 = `hmac sha384`
	CODE_METHOD_HS512 = `hmac sha512`
	CODE_METHOD_RS256 = `rsa sha256`
	CODE_METHOD_RS384 = `rsa sha384`
	CODE_METHOD_RS512 = `rsa sha512`
	CODE_METHOD_ES256 = `ecdsa sha256`
	CODE_METHOD_ES384 = `ecdsa sha384`
	CODE_METHOD_ES512 = `ecdsa sha512`
	CODE_METHOD_PS256 = `rsassa-pss sha256`
	CODE_METHOD_PS384 = `rsassa-pss sha384`
	CODE_METHOD_PS512 = `rsassa-pss sha512`
)
