package model

import (
	"encoding/json"
	"errors"

	"bitbucket.org/lebronto_kerovol/gwjwt/v1/constants"
	"bitbucket.org/lebronto_kerovol/gwjwt/v1/types"

	"bitbucket.org/lebronto_kerovol/gwerror"
	"github.com/golang-jwt/jwt/v4"
)

const (
	ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_KEY                        = `ERROR: Exception on - Get certificat and key.`
	ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_PUBLIC_KEY_AND_PRIVATE_KEY = `ERROR: Exception on - Get certificat, public key and private key.`
)

const (
	GWJWT_MODEL_KEY_PATH_ROUTE = `gwjwt/v1/model/key`
)

var (
	errorExceptionOnGetCertificatAndKey                    = gwerror.NewErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_KEY)
	errorExceptionOnGetCertificatAndPublicKeyAndPrivateKey = gwerror.NewErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_PUBLIC_KEY_AND_PRIVATE_KEY)
)

func getErrorExceptionOnGetCertificatAndKey() *gwerror.Error {
	return errorExceptionOnGetCertificatAndKey
}

func getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey() *gwerror.Error {
	return errorExceptionOnGetCertificatAndPublicKeyAndPrivateKey
}

type Key struct {
	jwtSigningMethod jwt.SigningMethod
	certificat       interface{}
	// Private key
	signKey interface{}
	// Public Key
	verifyKey interface{}
}

const (
	JSON_CERTIFICAT  = `certificat`
	JSON_KEY         = "key"
	JSON_PRIVATE_KEY = `private_key`
	JSON_PUBLIC_KEY  = `public_key`
)

func getCertificatAndKey(dict *map[string]interface{}) (string, string, error) {
	fieldCertificat, ok := (*dict)[JSON_CERTIFICAT]
	if !ok {
		return ``, ``, errors.New(`ERROR: JSON field "certificat" is not exist.`)
	}
	certificat, ok := fieldCertificat.(string)
	if !ok {
		return ``, ``, errors.New(`ERROR: Type "certificat" is invalid.`)
	}

	fieldKey, ok := (*dict)[JSON_KEY]
	if !ok {
		return ``, ``, errors.New(`ERROR: JSON field "key" is not exist.`)
	}
	key, ok := fieldKey.(string)
	if !ok {
		return ``, ``, errors.New(`ERROR: Type "key" is invalid.`)
	}

	return certificat, key, nil
}

func getCertificatAndPublicKeyAndPrivateKeyFromDict(dict *map[string]interface{}) (string, string, string, error) {
	fieldCertificat, ok := (*dict)[JSON_CERTIFICAT]
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: JSON field "certificat" is not exist.`)
	}
	certificat, ok := fieldCertificat.(string)
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: Type "certificat" is invalid.`)
	}

	fieldPublicKey, ok := (*dict)[JSON_PUBLIC_KEY]
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: JSON field "public_key" is not exist.`)
	}
	publicKey, ok := fieldPublicKey.(string)
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: Type "public_key" is invalid.`)
	}

	fieldPrivateKey, ok := (*dict)[JSON_PRIVATE_KEY]
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: JSON field "private_key" is not exist.`)
	}
	privateKey, ok := fieldPrivateKey.(string)
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: Type "private_key" is invalid.`)
	}

	return certificat, publicKey, privateKey, nil
}

func NewKeyFromJSON(codeAlgorithm string, sourceJson []byte) (types.Key, *gwerror.Error) {
	if codeAlgorithm == `` {
		return nil, gwerror.NewError(``, GWJWT_MODEL_KEY_PATH_ROUTE, errors.New(`ERROR: Parameter "codeAlgorithm" is empty.`))
	}

	var dict map[string]interface{}
	if err := json.Unmarshal(sourceJson, &dict); err != nil {
		return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, errors.New(`ERROR: Parameter "sourceJson" is invalid.`), err)
	}

	switch codeAlgorithm {
	case constants.CODE_METHOD_HS256:
		{
			certificat, key, err := getCertificatAndKey(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndKey(), err)
			}

			signKey := []byte(key)

			return &Key{jwtSigningMethod: jwt.SigningMethodHS256, certificat: certificat, signKey: signKey, verifyKey: signKey}, nil
		}
	case constants.CODE_METHOD_HS384:
		{
			certificat, key, err := getCertificatAndKey(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndKey(), err)
			}

			signKey := []byte(key)

			return &Key{jwtSigningMethod: jwt.SigningMethodHS384, certificat: certificat, signKey: signKey, verifyKey: signKey}, nil
		}
	case constants.CODE_METHOD_HS512:
		{
			certificat, key, err := getCertificatAndKey(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndKey(), err)
			}

			signKey := []byte(key)

			return &Key{jwtSigningMethod: jwt.SigningMethodHS512, certificat: certificat, signKey: signKey, verifyKey: signKey}, nil
		}
	case constants.CODE_METHOD_RS256:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_RS256".`, err)
			}

			verifyKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_RS256".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodRS256, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_RS384:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_RS384".`, err)
			}

			verifyKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_RS384".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodRS384, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_RS512:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_RS512".`, err)
			}

			verifyKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_RS512".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodRS512, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_PS256:
		return nil, gwerror.NewErrorFromString(``, ``, `ERROR: JWT algorithm is not support.`)
		{

			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_PS256".`, err)
			}

			verifyKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_PS256".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodPS256, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_PS384:
		return nil, gwerror.NewErrorFromString(``, ``, `ERROR: JWT algorithm is not support.`)
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_PS384".`, err)
			}

			verifyKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_PS384".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodPS384, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_PS512:
		return nil, gwerror.NewErrorFromString(``, ``, `ERROR: JWT algorithm is not support.`)
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_PS512".`, err)
			}

			verifyKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_PS512".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodPS512, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_ES256:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseECPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_ES256".`, err)
			}

			verifyKey, err := jwt.ParseECPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_ES256".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodES256, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_ES384:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseECPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_ES384".`, err)
			}

			verifyKey, err := jwt.ParseECPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_ES384".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodES384, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	case constants.CODE_METHOD_ES512:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_MODEL_KEY_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			signKey, err := jwt.ParseECPrivateKeyFromPEM([]byte(privateKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_ES512".`, err)
			}

			verifyKey, err := jwt.ParseECPublicKeyFromPEM([]byte(publicKey))
			if err != nil {
				return nil, gwerror.WrapErrorFromString(``, GWJWT_MODEL_KEY_PATH_ROUTE, `ERROR: In "CODE_METHOD_ES512".`, err)
			}

			return &Key{jwtSigningMethod: jwt.SigningMethodES512, certificat: certificat, signKey: signKey, verifyKey: verifyKey}, nil
		}
	}

	return nil, gwerror.NewErrorFromString(``, ``, `ERROR: Code algorithm is not supported.`)
}

func NewSymmetricKey(jwtSigningMethod jwt.SigningMethod, certificat interface{}, key interface{}) types.Key {
	return &Key{jwtSigningMethod: jwtSigningMethod, certificat: certificat, signKey: key, verifyKey: key}
}

func NewAsymmetricKey(jwtSigningMethod jwt.SigningMethod, certificat interface{}, signKey interface{}, verifyKey interface{}) types.Key {
	return &Key{jwtSigningMethod: jwtSigningMethod, certificat: certificat, signKey: signKey, verifyKey: verifyKey}
}

func (key *Key) GetSigningMethod() jwt.SigningMethod {
	return key.jwtSigningMethod
}

func (key *Key) GetCertificat() interface{} {
	return key.certificat
}

func (key *Key) GetSignKey() interface{} {
	return key.signKey
}

func (key *Key) GetVerifyKey() interface{} {
	return key.verifyKey
}
