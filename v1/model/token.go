package model

import (
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type ChainOfToken struct {
	AccountId int64 `pg:"account_idx"`
	// Recipient for which the JWT is intended.
	Audience string `pg:"audience"`
	// Time after which the JWT expires.
	ExpiresAt time.Time `pg:"expires_at"`
	// Unique identifier; can be used to prevent the JWT from being replayed (allows a token to be used only once).
	Id string `pg:"id"`
	// Time at which the JWT was issued; can be used to determine age of the JWT.
	IssuedAt time.Time `pg:"issued_at"`
	// Issuer of the JWT.
	Issuer string `pg:"issuer"`
	// Time before which the JWT must not be accepted for processing.
	NotBefore time.Time `pg:"not_before"`
	// Subject of the JWT (the user).
	Subject string `pg:"subject"`
	// Unique identificator entrypoint User system.
	Fingerprint string `pg:"fingerprint"`
	// IP connecting.
	IP string `pg:"ip"`
	// Code auth social network service.
	CodeAuthSocialNetworke string
	// Result token.
	Token string `pg:"token"`
}

type ClaimsOfToken struct {
	AccountId int64 `json:"accountId" pg:"account_idx"`
	// Unique identificator entrypoint User system
	Fingerprint string `json:"fingerprint" pg:"fingerprint"`
	// IP connecting
	IP string `json:"ip" pg:"ip"`
	// Standart claims
	jwt.StandardClaims
}

func (chainOfToken *ChainOfToken) GetClaims() ClaimsOfToken {
	return ClaimsOfToken{
		chainOfToken.AccountId,
		chainOfToken.Fingerprint,
		chainOfToken.IP,
		jwt.StandardClaims{
			Audience:  (*chainOfToken).Audience,
			ExpiresAt: (*chainOfToken).IssuedAt.Unix() + ((*chainOfToken).ExpiresAt.Unix() - (*chainOfToken).IssuedAt.Unix()),
			Id:        (*chainOfToken).Id,
			IssuedAt:  (*chainOfToken).IssuedAt.Unix(),
			Issuer:    (*chainOfToken).Issuer,
			NotBefore: (*chainOfToken).NotBefore.Unix(),
			Subject:   (*chainOfToken).Subject,
		},
	}
}
