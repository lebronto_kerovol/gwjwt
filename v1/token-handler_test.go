package v1_test

import (
	"crypto/rsa"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"strconv"
	"testing"
	"time"

	util "bitbucket.org/lebronto_kerovol/gwjwt/v1"
	"bitbucket.org/lebronto_kerovol/gwjwt/v1/constants"

	"github.com/golang-jwt/jwt/v4"
)

func TestHSGenerateTokenAndParse(t *testing.T) {
	type CustomClaims struct {
		FirstName string
		jwt.StandardClaims
	}

	keyClaims := CustomClaims{
		`Gaben`,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + 15000,
		},
	}
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, keyClaims)
	t.Log(fmt.Sprintf("CLAIMS: %+v", claims))

	keyWord := `Patrol.`
	token, err := claims.SignedString([]byte(keyWord))
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
		return
	}
	t.Log(fmt.Sprintf("TOKEN: %+v", token))

	decodeToken, err := jwt.ParseWithClaims(token, &CustomClaims{}, func(*jwt.Token) (interface{}, error) { return []byte(keyWord), nil })
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
		return
	}
	t.Log(fmt.Sprintf("TOKEN: %+v", decodeToken))
	t.Log(fmt.Sprintf("Claims: %+v", decodeToken.Claims))
	t.Log(fmt.Sprintf("Header: %+v", decodeToken.Header))
	t.Log(fmt.Sprintf("Method: %+v", decodeToken.Method))
	t.Log(fmt.Sprintf("Raw: %+v", decodeToken.Raw))
	t.Log(fmt.Sprintf("Signature: %+v", decodeToken.Signature))
	t.Log(fmt.Sprintf("Valid: %+v", decodeToken.Valid))

	resultClaims, ok := decodeToken.Claims.(*CustomClaims)
	if !ok {
		t.Error(fmt.Sprintf("ERROR: Cast is invalid!"))
		return
	}

	t.Log(fmt.Sprintf("First Name: %+v", resultClaims.FirstName))
}

// location of the files used for signing and verification
const (
	privateKeyPath = "test/private.pem" // openssl genrsa -out private.pem 2048
	publicKeyPath  = "test/public.pem"  // openssl rsa -in private.pem -pubout -out public.pem
)

const (
	privateKeyRSA256 = `2d2d2d2d2d424547494e205253412050524956415445204b45592d2d2d2d2d0a4d4949457067494241414b4341514541372f4a655531335250624768424d61643476654d432f4161656866664d7365573033707843573071454b6872507a4b430a6333664f753164556a33634849647055486f555a754d6a664a69447444794a2b6662693146497841315049492b42492f4e2b6f6455486e4b713154557a6264430a526a5366676535514d6c6f71764168776b7374523455706352712f4e6a5a624569526e33396f4944766e71346d6d6456564c4354754f46666f613359785372560a6d7831476f446d30515a4d4c6b796e6635637a47736a71685447784e47346343356e4f6630584d6a5047597276466d54573467635965686234714577514f54450a3669632f67637962524f445838317a745a38594e41444f6c516d765949646e76514c5a6f645654394a39387a4858652b36377842316c4947796732336e4532720a564d44636669744b6b776a6e66747548614b6a41627766384a647046576e303164436f692b77494441514142416f49424151436b4e64654e38432f4a712b334a0a6166415671317068745345356d32713577745161794c6e68504a39454a533634596d5861686779714d3173416a2f49476e666468703639787761374e316b7a470a52644952556a3543344c5668786e365333314836667a4d757078736e592f663744556243367a4b615a6a446d4e4d543453454a61672b4432634d427a383836560a377139304d4a78395448387064613075706a64457255393767476e464a2f5262455370686a5a4275386c347658444f3258374d7272734b685467375155726d790a62696237415765356937766d7a476c5434724b4146784e476d2f7163466c48716865465a306769586d766854594339336e3170666c36657056676c636734546a0a427835332b73767458546b4a5658774f6a38315743544238516532574136624144726a44764256366b4f36766c574759534d686f374f627036784873726746620a566d473432664942416f474241507a77734b772f387738702b69657834663257784b6b7a325559625a45627a6b635941466c2b6946316e464d566b78424f72310a574d61536d44376e774a6c2b4a5a4e6b62517465394b424e506d422f757065333755396c3937484c3562575153646948596b54426a5678385130376545482f520a7246466236745851766d46334c746f4863376e394658522b4e754265737547575772725639417a4332614f354d5a543062663874534c3337416f474241504c5a0a634b4e754947485955346743446d324d55684b45574b705563366f695576544e35525236364661737954464b49703771435a4954614b3448746e507162774b4b0a5a76715638416261356e416f706e365038385a2b49676c6c42794352683957692b4e594c374a357a6375715a625a64766f2b3541494f327755716277655141410a526d46635a416a723571503579502f76487a74663079704d6274676b73672b67485756426f423842416f4742414b31447676497657444e53414f384346686c640a467467464556664a326259556b51323937355751396b666a4958764562576235713964536767484e5251796638654e3254704155657150345a6273366f376e630a487935645867416e2b504758706b347835796e4d49435a7a5a4c5034762f716579566875433335743449767056576d3634785a4c66452f7853577747307168660a45305761322f4f4634676f614352484e496a4244642b6e54416f4742414b5743344b7669454a7666446638794e3563642f596d315061444938685051564f32580a726a747045525352716f57335779556e714f49763744592f39322f464a57643734486545335251752b4d4d555558554d737a356a464e75475358632b2b4737340a4a645073534a59745a504c5449736746524a724b686961343933766a6850516c4f394a3758347064444e4a524756616c4b496f553478782b2f3053387263624d0a6633785658385942416f4742414f516854662b3462776562304c664f4c75585072654f756849774643646f74466441357378624f3447373235572f544a774d780a6c4e4b594437544b6c372f7247726b4d7245506252396a735654794546462f446937584748394263777435635a6c594b7979436e5876517945764d6f6a39304c0a584e3739695a5763355a534935714d7a2b347978666f51383951524e354b63737a6938584a503950786a3970776c5765494d756d566c4e740a2d2d2d2d2d454e44205253412050524956415445204b45592d2d2d2d2d0a`
	publicKeyRSA256  = `2d2d2d2d2d424547494e205055424c4943204b45592d2d2d2d2d0a4d494942496a414e42676b71686b6947397730424151454641414f43415138414d49494243674b4341514541372f4a655531335250624768424d61643476654d0a432f4161656866664d7365573033707843573071454b6872507a4b436333664f753164556a33634849647055486f555a754d6a664a69447444794a2b666269310a46497841315049492b42492f4e2b6f6455486e4b713154557a626443526a5366676535514d6c6f71764168776b7374523455706352712f4e6a5a624569526e330a396f4944766e71346d6d6456564c4354754f46666f613359785372566d7831476f446d30515a4d4c6b796e6635637a47736a71685447784e47346343356e4f660a30584d6a5047597276466d54573467635965686234714577514f54453669632f67637962524f445838317a745a38594e41444f6c516d765949646e76514c5a6f0a645654394a39387a4858652b36377842316c4947796732336e453272564d44636669744b6b776a6e66747548614b6a41627766384a647046576e303164436f690a2b774944415141420a2d2d2d2d2d454e44205055424c4943204b45592d2d2d2d2d0a`
)

// read the key files before starting http handlers
func getKeys() (*rsa.PrivateKey, *rsa.PublicKey, error) {
	signBytes, err := hex.DecodeString(privateKeyRSA256)
	if err != nil {
		return nil, nil, err
	}

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return nil, nil, err
	}

	verifyBytes, err := hex.DecodeString(publicKeyRSA256)
	if err != nil {
		return nil, nil, err
	}

	verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		return nil, nil, err
	}

	return signKey, verifyKey, nil
}

// read the key files before starting http handlers.
func getKeysFromFile() (*rsa.PrivateKey, *rsa.PublicKey, error) {
	signBytes, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		return nil, nil, err
	}

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return nil, nil, err
	}

	verifyBytes, err := ioutil.ReadFile(publicKeyPath)
	if err != nil {
		return nil, nil, err
	}

	verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		return nil, nil, err
	}

	return signKey, verifyKey, nil
}

func TestRSGenerateTokenAndParse(t *testing.T) {
	type CustomClaims struct {
		FirstName string
		jwt.StandardClaims
	}

	keyClaims := CustomClaims{
		`Gaben`,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + 15000,
		},
	}
	claims := jwt.NewWithClaims(jwt.SigningMethodRS256, keyClaims)
	t.Log(fmt.Sprintf("CLAIMS: %+v", claims))

	singKey, verifyKey, err := getKeys()
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
	}

	token, err := claims.SignedString(singKey)
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
	}
	t.Log(fmt.Sprintf("TOKEN: %+v", token))

	decodeToken, err := jwt.ParseWithClaims(token, &CustomClaims{}, func(*jwt.Token) (interface{}, error) { return verifyKey, nil })
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
	}
	t.Log(fmt.Sprintf("TOKEN: %+v", decodeToken))
	t.Log(fmt.Sprintf("Claims: %+v", decodeToken.Claims))

	resultClaims, ok := decodeToken.Claims.(*CustomClaims)
	if !ok {
		t.Error(fmt.Sprintf("ERROR: Cast is invalid!"))
		return
	}

	t.Log(fmt.Sprintf("First Name: %+v", resultClaims.FirstName))
}

func TestRSGenerateTokenAndParseFromFile(t *testing.T) {
	type CustomClaims struct {
		FirstName string
		jwt.StandardClaims
	}

	keyClaims := CustomClaims{
		`Gaben`,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + 15000,
		},
	}
	claims := jwt.NewWithClaims(jwt.SigningMethodRS256, keyClaims)
	t.Log(fmt.Sprintf("CLAIMS: %+v", claims))

	singKey, verifyKey, err := getKeysFromFile()
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
	}

	token, err := claims.SignedString(singKey)
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
	}
	t.Log(fmt.Sprintf("TOKEN: %+v", token))

	decodeToken, err := jwt.ParseWithClaims(token, &CustomClaims{}, func(*jwt.Token) (interface{}, error) { return verifyKey, nil })
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
	}
	t.Log(fmt.Sprintf("TOKEN: %+v", decodeToken))
	t.Log(fmt.Sprintf("Claims: %+v", decodeToken.Claims))

	resultClaims, ok := decodeToken.Claims.(*CustomClaims)
	if !ok {
		t.Error(fmt.Sprintf("ERROR: Cast is invalid!"))
	}

	t.Log(fmt.Sprintf("First Name: %+v", resultClaims.FirstName))
}

type Account struct {
	id int64
}

func (ac *Account) GetId() int64 {
	return ac.id
}

func (ac *Account) ToDebugString() string {
	return fmt.Sprintf("Account: %+v", ac)
}

func TestGenerateTokenAndParseUtil(t *testing.T) {
	fingerprint := `1g21g32tz`
	ip := `1.1.1.1:1`
	socialNetwork := "test-social-network"

	account := &Account{id: 1}
	chain, claims := util.GenerateChainWithClaims(jwt.SigningMethodHS256, account.id, fingerprint, ip, socialNetwork, ``, strconv.FormatInt(account.GetId(), 10), ``, ``, time.Now().Add(time.Hour*24), time.Now(), time.Now())
	t.Log(fmt.Sprintf("CHAIN: %+v", chain))
	t.Log(fmt.Sprintf("CLAIMS: %+v", claims))

	keyWord, gwerr := util.NewKeyFromJSON(constants.CODE_METHOD_HS256, []byte(`{"certificat":"", "key":"123455125"}`))
	if gwerr != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", gwerr))
		return
	}

	token, err := util.SignedString(claims, keyWord)
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
		return
	}
	t.Log(fmt.Sprintf("TOKEN: %+v", *token))

	decodeClaims, err := util.ParseTokenToClaims(*token, keyWord, nil) //decodeToken, err := jwt.ParseWithClaims(*token, &model.ValidationClaimsOfToken{}, func(*jwt.Token) (interface{}, error) { return []byte(keyWord), nil })
	if err != nil {
		t.Error(fmt.Sprintf("ERROR: %+v", err))
		return
	}
	//t.Log(fmt.Sprintf("TOKEN: %+v", decodeToken))
	t.Log(fmt.Sprintf("Claims: %+v", decodeClaims))
}
