package v1

import (
	"errors"
	"time"

	"bitbucket.org/lebronto_kerovol/gwjwt/v1/model"
	"bitbucket.org/lebronto_kerovol/gwjwt/v1/types"

	"github.com/golang-jwt/jwt/v4"
)

func GenerateChain(accountId *int64, fingerprint, ip, codeAuthSocialNetwork, audience, id, issuer, subject *string, expiresAt, issuedAt, notBefore *time.Time) *model.ChainOfToken {
	chain := model.ChainOfToken{
		AccountId:              *accountId,
		Audience:               *audience,
		ExpiresAt:              *expiresAt, //time.Now().Add(time.Hour * 24),
		Id:                     *id,        //strconv.FormatInt(account.GetId(), 10),
		IssuedAt:               *issuedAt,  //time.Now(),
		Issuer:                 *issuer,
		NotBefore:              *notBefore, //time.Now(),
		Subject:                *subject,
		Fingerprint:            *fingerprint,
		IP:                     *ip,
		CodeAuthSocialNetworke: *codeAuthSocialNetwork,
		Token:                  "",
	}

	return &chain
}

func GenerateChainWithClaims(signinMethod jwt.SigningMethod, accountId int64, fingerprint, ip, codeAuthSocialNetwork, audience, id, issuer, subject string, expiresAt, issuedAt, notBefore time.Time) (*model.ChainOfToken, *jwt.Token) {
	chain := GenerateChain(&accountId, &fingerprint, &ip, &codeAuthSocialNetwork, &audience, &id, &issuer, &subject, &expiresAt, &issuedAt, &notBefore)
	claims := jwt.NewWithClaims(signinMethod, chain.GetClaims())

	return chain, claims
}

func SignedString(claims *jwt.Token, key types.Key) (*string, error) {
	token, err := claims.SignedString(key.GetSignKey())
	if err != nil {
		return nil, err
	}

	return &token, nil
}

func SigningString(chain *model.ChainOfToken, claims *jwt.Token) (*string, error) {
	token, err := claims.SigningString()
	if err != nil {
		return nil, err
	}

	return &token, nil
}

func ParseTokenToClaims(tokenString string, key types.Key, keyFunc *jwt.Keyfunc) (*model.ClaimsOfToken, error) {
	var token *jwt.Token
	var err error

	if keyFunc != nil {
		token, err = jwt.ParseWithClaims(tokenString, &model.ClaimsOfToken{}, *keyFunc)
	} else {
		token, err = jwt.ParseWithClaims(tokenString, &model.ClaimsOfToken{}, func(token *jwt.Token) (interface{}, error) {
			return key.GetVerifyKey(), nil
		})
	}

	if claims, ok := token.Claims.(*model.ClaimsOfToken); ok && token.Valid {
		return claims, nil
	} else {
		return nil, err
	}
}

func ParseTokenErrorChecking(tokenString string, key types.Key) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return key.GetVerifyKey(), nil
	})

	if token.Valid {
		return nil //Pass
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return errors.New(`ERROR: In parameter "tokenString" - value is invalid. It is not token.`)
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			return errors.New(`ERROR: Token is either expired or not active yet.`)
		} else {
			return err
		}
	} else {
		return err
	}
}
