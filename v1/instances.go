package v1

import (
	"bitbucket.org/lebronto_kerovol/gwjwt/v1/model"
	"bitbucket.org/lebronto_kerovol/gwjwt/v1/types"

	"bitbucket.org/lebronto_kerovol/gwerror"
	"github.com/golang-jwt/jwt/v4"
)

func NewKeyFromJSON(codeAlgorithm string, sourceJson []byte) (types.Key, *gwerror.Error) {
	return model.NewKeyFromJSON(codeAlgorithm, sourceJson)
}

func NewSymmetricKey(jwtSigningMethod jwt.SigningMethod, certificat interface{}, key interface{}) types.Key {
	return model.NewSymmetricKey(jwtSigningMethod, certificat, key)
}

func NewAsymmetricKey(jwtSigningMethod jwt.SigningMethod, certificat interface{}, signKey interface{}, verifyKey interface{}) types.Key {
	return model.NewAsymmetricKey(jwtSigningMethod, certificat, signKey, verifyKey)
}
