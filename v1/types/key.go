package types

import "github.com/golang-jwt/jwt/v4"

type Key interface {
	GetSigningMethod() jwt.SigningMethod
	GetCertificat() interface{}
	GetSignKey() interface{}
	GetVerifyKey() interface{}
}
